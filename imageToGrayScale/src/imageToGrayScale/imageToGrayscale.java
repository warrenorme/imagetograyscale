package imageToGrayScale;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;
public class imageToGrayscale {

	public static void main(String[] args) {
		if (args.length == 1) {
		File file = new File(args[0]);
		try {
			BufferedImage image = ImageIO.read(file);
			for (int x=0;x<image.getWidth();++x) 
			for (int y=0;y<image.getHeight();++y) {
					int rgbvalue = image.getRGB(x,y);
					int r = (rgbvalue>>16)&0xff;
					int g = (rgbvalue>>8)&0xff;
					int b = rgbvalue&0xff;
					float gamma = (float) 2.2;
					// Normalise the number to a value between 0-1 and 2.2 to deal with gamma compression
					float rnormal = (float) Math.pow(r/255,gamma);
					float gnormal = (float) Math.pow(g/255, gamma);
					float bnormal = (float) Math.pow(b/255, gamma);
					// Uses the weighted linear equeation to get a luminsoty value
					double lum = (rnormal*0.2126) + (0.7152*gnormal) + (0.0722*bnormal);
					// Readdeds gamma compression
					int lumgamma = (int) (255 * Math.pow(lum,1/gamma));
					int gray =  (lumgamma<<16) + (lumgamma<<8) + lumgamma;
					image.setRGB(x,y,gray);
			}
			ImageIO.write(image, "png", file);
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		else {
			System.out.print("Incorrect Number of Args");
		}
	}

}
